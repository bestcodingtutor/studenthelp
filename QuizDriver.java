//codifytutor.com

import java.util.Scanner;

public class QuizDriver {
    private Quiz quiz = new Quiz();
    Scanner sc = new Scanner(System.in);

    //the main method. Starts application here.
    public static void main(String[] args){
        QuizDriver quizDriver = new QuizDriver();
        quizDriver.processRequest();
    }
    //lets process the user options here1
    public void processRequest(){
        System.out.println("");
        System.out.println("Please choose an option:");
        System.out.println("1: Add Question ");
        System.out.println("2: Start Quiz");
        System.out.println("");
        int choice = Integer.parseInt(sc.nextLine());


        if(choice==1){
            Question question = quiz.constructQuestion();
            quiz.add(question);
            //show the menu again
            this.processRequest();
        }else {
            //give the quiz and show the final score
            quiz.giveQuiz();
            quiz.showFinalScore();
            sc.close();
        }

    }
}
