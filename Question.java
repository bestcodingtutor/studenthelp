public class Question {
    private String questionContent;
    private String answer;

    public Question(String questionContent,String answer){
        this.questionContent= questionContent;
        this.answer = answer;
    }

    public String getQuestion(){
        return this.questionContent;
    }

    public boolean answerCorrect(String given){
        return this.answer.equals(given);
    }

}