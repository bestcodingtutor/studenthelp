//codifytutor.com

import java.util.Scanner;
public class Quiz
{
    private final int MAX_QUESTIONS = 25;
    private Question[] questions;
    private int current;
    private int correct;
    private int incorrect;
    Scanner sc = new Scanner(System.in);

    public Quiz()
    {
        questions = new Question[MAX_QUESTIONS];
        current = 0;
        correct = incorrect = 0;
    }
    public void add(Question newQuestion)
    {
        if (current < MAX_QUESTIONS) {
            questions[current++] = newQuestion;
        }
    }

    //the question needs to be constructed
    public Question constructQuestion(){
        //request the user to add a question

        System.out.println("");
        System.out.println("Please enter a question: ");
        String question = sc.nextLine();
        System.out.println("Please enter the answer: ");
        String answer = sc.nextLine();


        return new Question(question,answer);
    }

    public void giveQuiz()
    {
        Scanner sc = new Scanner (System.in);
        for (int i = 0; i < current; i++)
        {
            System.out.println(questions[i].getQuestion());
            if (questions[i].answerCorrect(sc.nextLine()))
                correct++;
            else
                incorrect++;
        }
        sc.close();
    }
    public int getNumCorrect()
    {
        return correct;
    }
    public int getNumIncorrect()
    {
        return incorrect;
    }

    public void showFinalScore(){
        System.out.println("");
        System.out.println("Your final score are: ");
        System.out.println("Correct questions: "+correct);
        System.out.println("Incorrect questions: "+incorrect);
        System.out.println("");
    }
}
